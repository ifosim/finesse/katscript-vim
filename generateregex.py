import pprint
from finesse.script.spec import KatSpec

spec = KatSpec()

commands = [*spec.commands, *spec.analyses]
commands.sort(key = lambda item: (-len(item), item))

elements = list(spec.elements)
elements.sort(key = lambda item: (-len(item), item))

functions = list(spec.expression_functions)
functions.sort(key = lambda item: (-len(item), item))

keywords = list(spec.reserved_names)
keywords.sort(key = lambda item: (-len(item), item))

with open("template.vim") as file:
    template = file.read()

with open("syntax/kat.vim", "w") as file:
    file.write(template)

    file.write(f"syn keyword katCommand contained nextgroup=katCommandArgs skipwhite\n\t\\ ")
    file.write("\n\t\\ ".join(commands) + "\n\n")

    file.write(f"syn keyword katElement contained nextgroup=katName skipwhite\n\t\\ ")
    file.write("\n\t\\ ".join(elements) + "\n\n")

    file.write(f"syn keyword katFunction contained\n\t\\ ")
    file.write("\n\t\\ ".join(functions) + "\n\n")

    file.write(f"syn match katKeyword contained display /[A-Za-z0-9._]\@<!\(")
    file.write("\|".join(keywords) + "\)\>/\n")
